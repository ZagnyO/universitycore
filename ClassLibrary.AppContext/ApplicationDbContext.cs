﻿using University.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ClassUniversity.AppContext
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<ClassTime> ClassTime { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<TeachSubj> TeachSubjs { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Schedule> Schedules { get; set; }

        public ApplicationDbContext(DbContextOptions options): base(options)
        {
            Database.EnsureCreated();
        }
        
    }
}
