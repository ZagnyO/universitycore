﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface IStudentsRepository : IDbRepository<Student>
    {

    }
    public class StudentsRepository : GenericRepository<Student>, IStudentsRepository
    {
        public StudentsRepository(DbContext context) :base(context)
        {

        }
    }
}
