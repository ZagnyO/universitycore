﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface IMarksRepository : IDbRepository<Mark>
    {

    }
    public class MarksRepository : GenericRepository<Mark>, IMarksRepository
    {
        public MarksRepository(DbContext context) : base(context)
        {

        }
    }
}
