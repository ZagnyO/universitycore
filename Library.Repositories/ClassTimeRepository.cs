﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface IClassTimeRepository: IDbRepository<ClassTime>
    {

    }
    public class ClassTimeRepository : GenericRepository<ClassTime>, IClassTimeRepository
    {
        public ClassTimeRepository(DbContext context) : base(context)
        {
        }
    }
}
