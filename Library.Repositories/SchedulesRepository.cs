﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface ISchedulesRepository : IDbRepository<Schedule>
    {

    }
    public class SchedulesRepository : GenericRepository<Schedule>, ISchedulesRepository
    {
        public SchedulesRepository(DbContext context) : base(context)
        {
        }
    }
}
