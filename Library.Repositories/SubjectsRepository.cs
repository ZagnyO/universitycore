﻿
using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface ISubjectsRepository : IDbRepository<Subject>
    {

    }
    public class SubjectsRepository : GenericRepository<Subject>, ISubjectsRepository
    {

        public SubjectsRepository(DbContext context): base(context)
        {

        }
    }
}
