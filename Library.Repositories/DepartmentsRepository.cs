﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface IDepartmentsRepository : IDbRepository<Department>
    {

    }
    public class DepartmentsRepository : GenericRepository<Department>, IDepartmentsRepository
    {
        public DepartmentsRepository(DbContext context) : base(context)
        {
        }
    }
}
