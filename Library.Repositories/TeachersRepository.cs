﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface ITeachersRepository : IDbRepository<Teacher>
    {

    }
    public class TeachersRepository : GenericRepository<Teacher>, ITeachersRepository
    {
        public TeachersRepository(DbContext context) : base(context)
        {
        }
    }
}
