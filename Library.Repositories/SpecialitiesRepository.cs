﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface ISpecialitiesRepository : IDbRepository<Speciality>
    {

    }
    public class SpecialitiesRepository : GenericRepository<Speciality>, ISpecialitiesRepository
    {
        public SpecialitiesRepository(DbContext context): base(context)
        {
        }
    }
}
