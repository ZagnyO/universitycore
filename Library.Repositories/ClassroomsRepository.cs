﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface IClassroomsRepository: IDbRepository<Classroom>
    {

    }
    public class ClassroomsRepository : GenericRepository<Classroom>, IClassroomsRepository
    {
        public ClassroomsRepository(DbContext context) : base(context)
        {
        }
    }
   
}
