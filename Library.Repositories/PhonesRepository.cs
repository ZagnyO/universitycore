﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface IPhonesRepository : IDbRepository<Phone>
    {

    }
    public class PhonesRepository : GenericRepository<Phone>, IPhonesRepository
    {
        public PhonesRepository(DbContext context) : base(context)
        {
        }
    }
}
