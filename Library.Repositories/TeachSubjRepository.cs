﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface ITeachSubjRepository : IDbRepository<TeachSubj>
    {

    }
    public class TeachSubjRepository : GenericRepository<TeachSubj>, ITeachSubjRepository
    {
        public TeachSubjRepository(DbContext context) : base(context)
        {
        }
    }
}
