﻿using University.Domain;
using University.Entities;
using Microsoft.EntityFrameworkCore;

namespace University.Repositories
{
    public interface ILecturesRepository : IDbRepository<Lecture>
    {

    }
    public class LecturesRepository : GenericRepository<Lecture> , ILecturesRepository
    {
        public LecturesRepository(DbContext context) : base (context)
        {

        }
    }
}
