﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Entities;
using University.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ClassUniversity.AppContext;

namespace University.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        ApplicationDbContext db;
        public StudentController(ApplicationDbContext context)
        {
            db = context;
            if (!db.Students.Any())
            {
                //db.Students.Add(new Product { Name = "iPhone X", Company = "Apple", Price = 79900 });
                //db.Products.Add(new Product { Name = "Galaxy S8", Company = "Samsung", Price = 49900 });
                //db.Products.Add(new Product { Name = "Pixel 2", Company = "Google", Price = 52900 });
                //db.SaveChanges();
            }
        }
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            return db.Students.ToList();
        }

        [HttpGet("{id}")]
        public Student Get(int id)
        {
            Student student = db.Students.FirstOrDefault(x => x.Id == id);
            return student;
        }

        [HttpPost]
        public IActionResult Post([FromBody]Student student)
        {
            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return Ok(student);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Student student)
        {
            if (ModelState.IsValid)
            {
                db.Update(student);
                db.SaveChanges();
                return Ok(student);
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Student student = db.Students.FirstOrDefault(x => x.Id == id);
            if (student != null)
            {
                db.Students.Remove(student);
                db.SaveChanges();
            }
            return Ok(student);
        }



        //////IStudentsRepository _repository;
        //////public StudentController(IStudentsRepository repository)
        //////{
        //////    _repository = repository;
        //////}
        //////[HttpGet]
        //////public IEnumerable<Student> ReadAll()
        //////{
        //////    return _repository.AllItems.AsEnumerable();
        //////}
    }
}