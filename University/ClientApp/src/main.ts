import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}

const providers = [
  { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] }
];

if (environment.production) {
  enableProdMode();
}
const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);
platformBrowserDynamic(providers).bootstrapModule(AppModule)
  .catch(err => console.log(err));

// для перезагузки приложения при изменениях в исходном коде
if (module.hot) {
  module.hot.accept();
  module.hot.dispose(() => {
    // Перед перезапуском приложения создаем новый элемент app, которым заменяем старый
    const oldRootElem = document.querySelector('app');
    const newRootElem = document.createElement('app');
    oldRootElem!.parentNode!.insertBefore(newRootElem, oldRootElem);
    platform.destroy();
  });
}
