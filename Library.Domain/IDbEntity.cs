﻿using System;
using System.ComponentModel.DataAnnotations;

namespace University.Domain
{
    public interface IDbEntity
    {
        [Key]
        int Id { get; set; }
    }
 }
