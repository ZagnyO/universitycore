﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("tbGroups")]
    public class Group : AbstractDbEntity
    {
        [StringLength(64)]
        public string Name { get; set; }
        public virtual Speciality Speciality { get; set; }
        public virtual List<Student> Students { get; set; }
        public virtual List<Schedule> Schedules { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
