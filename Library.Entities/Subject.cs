﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("tbSubjects")]
    public class Subject: AbstractDbEntity
    {
        [StringLength(64)]
        public string Name { get; set; }
        public virtual List<TeachSubj> TeachSubjs { get; set; }
        public override string ToString()
        {
            return $"  {Name}";
        }
    }
}
