﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("tbSchedule")]
    public class Schedule : AbstractDbEntity
    {
        public string DayWeek { get; set; }
        public virtual List<Lecture> Lectures { get; set; }
        //public Schedule()
        //{
        //    List<string> DayWeek = new List<string> {"Понедельник",
        //                                             "Вторник",
        //                                             "Среда",
        //                                             "Четверг",
        //                                             "Пятница",
        //                                             "Суббота",
        //                                             "Воскресенье"};
        //}
        public override string ToString()
        {
            return $"  {DayWeek}  ";
        }
    }
}
