﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("tbTeachSubj")]
    public class TeachSubj : AbstractDbEntity
    {
        public virtual Teacher Teacher { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual List<Mark> Marks { get; set; }
        public override string ToString()
        {
            return $"  {Teacher}  \t  {Subject}";
        }
    } 
}
