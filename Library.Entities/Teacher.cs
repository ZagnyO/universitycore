﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using University.Domain;


namespace University.Entities
{
    [Table("TbTeachers")]
    public class Teacher: AbstractDbEntity
    {
        [StringLength(64)]
        public string FirstName { get; set; }
        [StringLength(64)]
        public string MiddleName { get; set; }
        [StringLength(64)]
        public string LastName { get; set; }
        public virtual Department Department { get; set;}
        public virtual List<TeachSubj> TeachSubjs { get; set; }
        public override string ToString()
        {
            return $"  {LastName}  {FirstName.ToCharArray()[0]}{"."}{MiddleName.ToCharArray()[0]}{"."}";
        }
    }
}
