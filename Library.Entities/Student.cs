﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("tbStudents")]
    public class Student: AbstractDbEntity
    {
        [StringLength(64)]
        public string FirstName { get; set; }
        [StringLength(64)]
        public string MiddleName { get; set; }
        [StringLength(64)]
        public string LastName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Birthday { get; set; }
       
        public int LogbookNumber { get; set; }
        [StringLength(64)]
        public string Email { get; set; }
        [StringLength(128)]
        public string Address { get; set; }
        public virtual List<Phone> Phones { get; set; }
        public virtual Group Group { get; set; }
        public virtual List<Mark> Marks { get; set; }
        public override string ToString()
        {
            return $"{LastName}  {FirstName.ToCharArray()[0]}{"."}{MiddleName.ToCharArray()[0]}{"."}";
            // return $"{FirstName} {MiddleName} {LastName} {Group} {LogbookNumber}";
        }
    }
}
