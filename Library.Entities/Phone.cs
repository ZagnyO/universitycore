﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("tbPhones")]
    public class Phone : AbstractDbEntity
    {
        [StringLength(64)]
        public string StudentsPhone { get; set; }
        public virtual Student Student { get; set; }
        public override string ToString()
        {
            return StudentsPhone;
        }
    }
}
