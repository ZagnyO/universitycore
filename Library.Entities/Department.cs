﻿using System;
using System.Collections.Generic;
using University.Domain;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace University.Entities
{
    [Table ("TbDepurtments")]
    public class Department: AbstractDbEntity
    {
        [StringLength(64)]
        public string Name { get; set; }
        public virtual List<Teacher> Teachers { get; set;  }
        public override string ToString()
        {
            return Name;
        }
    } 
}
