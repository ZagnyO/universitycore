﻿using University.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace University.Entities
{
    [Table("TbClassrooms")]
    public class Classroom: AbstractDbEntity
    {
        public int ClassroomNumber { get; set; }
        public virtual List<Lecture> Lectures { get; set; }
        public override string ToString()
        {
            return $"{ClassroomNumber} ";
        }
    }
}
